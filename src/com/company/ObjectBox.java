package com.company;

/**
 * Задание 2. Создать класс ObjectBox, который будет хранить коллекцию Object.
 *
 *         У класса должен быть метод addObject, добавляющий объект в коллекцию.
 *         У класса должен быть метод deleteObject, проверяющий наличие объекта в коллекции и при наличии удаляющий его.
 *         Должен быть метод dump, выводящий содержимое коллекции в строку.
 */

import java.util.*;

public class ObjectBox {
    //НАСТАВНИК
    // почему Number? В задании не конкретизируется, какого типа должны быть объекты
    public List<Object> listNum = new ArrayList<>(); //список на выход

    @Override
    public String toString() {
        return listNum.toString();
    }

    @Override
    public int hashCode() {
        return listNum.hashCode();
    }

    //НАСТАВНИК
    // то же, что для первой задачи
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        ObjectBox obj2 = (ObjectBox) obj;
        return obj2.listNum.equals(this.listNum); // doc:  Returns true if and only if the specified object is also a list, both lists have the same size, and all corresponding pairs of elements in the two lists are equal.
    }

    // У класса должен быть метод addObject, добавляющий объект в коллекцию.
    public void addObject(Object e) {
        listNum.add(e);
    }


    //  У класса должен быть метод deleteObject, проверяющий наличие объекта в коллекции и при наличии удаляющий его.
    public void deleteObject(Object e) {
        Iterator<Object> it = listNum.iterator();
        while (it.hasNext()) {
            Object n = it.next();
            if (n.equals(e)) {
                it.remove();
            }
        }
    }

    //Должен быть метод dump, выводящий содержимое коллекции в строку.
    public String dump() {
        return listNum.toString();
    }

}
