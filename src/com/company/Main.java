package com.company;

/**
 * Задание 2. Создать класс ObjectBox, который будет хранить коллекцию Object.
 *
 *         У класса должен быть метод addObject, добавляющий объект в коллекцию.
 *         У класса должен быть метод deleteObject, проверяющий наличие объекта в коллекции и при наличии удаляющий его.
 *         Должен быть метод dump, выводящий содержимое коллекции в строку.
 */

public class Main {

    public static void main(String[] args) {
	// write your code here
        ObjectBox m = new ObjectBox();

        m.addObject(100);
        m.addObject(2);
        m.addObject(2);
        System.out.println("Добавили элементы': " + m);

        m.deleteObject(2); //удаляем элемент "2"
        System.out.println("Удалили элементы 2: " + m);
    }
}
